package main

import (
	"database/sql"
	"encoding/json"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

func main() {
	db, err := sql.Open("postgres", "host=localhost port=5433 dbname=dev_db user=root password=root sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}

	if err := db.Ping(); err != nil {
		log.Fatalln(err)
	}
	defer db.Close()

	for i := 1; i < 5; i++ {

		file := "btc-usdt/" + strconv.Itoa(i) + ".json"
		println(file)
		jsonFile, err := os.Open(file)
		if err != nil {
			log.Fatal(err.Error())
		}
		//println(file)

		byteValue, err := ioutil.ReadAll(jsonFile)
		if err != nil {
			log.Fatal(err.Error())
		}

		var d [][]interface{}
		//0 время начала 1 open 2 high 3 low 4 close 5 vol btc 6 end time 7 vol usdt

		err = json.Unmarshal([]byte(byteValue), &d)
		if err != nil {
			log.Fatal(file, "   ", err.Error())
		}
		println(len(d))

		for i := 0; i < len(d); i++ {
			id := 0
			err := db.QueryRow(
				`INSERT INTO btc_usdt(time_start, open, high, low, close, vol_1, time_end, vol_2)
						VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id`,
				d[i][0],
				d[i][1],
				d[i][2],
				d[i][3],
				d[i][4],
				d[i][5],
				d[i][6],
				d[i][7],
			).Scan(&id)
			if err != nil {
				log.Fatal(err)
			}
			//log.Printf("%v\n", d[i][0])
		}
		jsonFile.Close()
	}
}

//create table btc_usdt
//(
//time_start bigint                                      not null,
//open       numeric(20, 2)                              not null,
//high       numeric(20, 2)                              not null,
//low        numeric(20, 2)                              not null,
//close      numeric(20, 2)                              not null,
//vol_1      numeric(20, 2)                              not null,
//time_end   bigint                                      not null,
//vol_2      numeric(20, 2)                              not null,
//id         bigserial
//constraint btc_usdt_pk
//primary key,
//tf         varchar(16) default '1d'::character varying not null
//);
//
//alter table btc_usdt
//owner to root;
