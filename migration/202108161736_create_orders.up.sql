create table pay_methods
(
	id serial
		constraint pay_methods_pk
			primary key,
	identifier varchar(256) not null,
	payee varchar(256),
	bank varchar(256),
	bank_value varchar(256)
);


create table orders
(
	id serial
		constraint orders_pk
			primary key,
	order_number varchar(256) not null,
	create_time bigint not null,
	notify_pay_end_time bigint not null,
	price numeric(10,10),
	amount numeric(10,10) not null,
	asset varchar(64) not null,
	fiat varchar(64) not null,
	total_price numeric(10,10),
	trade_type varchar(16),
	pay_method_id bigint not null
);

alter table orders
	add constraint orders_pay_methods_id_fk
		foreign key (pay_method_id) references pay_methods
			on update restrict on delete restrict;