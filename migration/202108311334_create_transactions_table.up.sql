create table transactions
(
    id serial
        constraint transactions_pk
            primary key,
    order_no varchar(256) not null,
    fiat_currency varchar(64) not null,
    status varchar(64) not null,
    amount numeric(20,2) not null,
    created_at timestamp without time zone not null,
    updated_at timestamp without time zone not null
);