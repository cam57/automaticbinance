create table receipts
(
	id serial
		constraint receipts_pk
			primary key,
	number_receipt int not null,
	price numeric(20,2) not null,
	full_name varchar(128) not null,
	bank varchar(64) not null,
	file_name varchar(256) not null,
	receipt_time varchar(128) not null
);