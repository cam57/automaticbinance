package automaticbinance

var HeadersBinance map[string][]string
var HeadersTelegram map[string][]string

func init() {
	HeadersBinance = map[string][]string{
		"authority":          {"https://c2c.binance.com/ru/fiatOrder"},
		"x-trace-id":         {"8da3c5fc-1b5c-436d-b33d-ae8e27f47997"},
		"csrftoken":          {"c6abd8873f3b664739d68be7e287dfc2"},
		"x-ui-request-trace": {"8da3c5fc-1b5c-436d-b33d-ae8e27f47997"},
		"user-agent":         {"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36"},
		"content-type":       {"application/json"},
		"lang":               {"ru"},
		"fvideo-id":          {"31a42304c94aad2389868d0b9937af05cc576d72"},
		"device-info":        {"eyJzY3JlZW5fcmVzb2x1dGlvbiI6IjE5MjAsMTA4MCIsImF2YWlsYWJsZV9zY3JlZW5fcmVzb2x1dGlvbiI6IjE5MjAsMTA4MCIsInN5c3RlbV92ZXJzaW9uIjoiTGludXggeDg2XzY0IiwiYnJhbmRfbW9kZWwiOiJ1bmtub3duIiwic3lzdGVtX2xhbmciOiJydS1SVSIsInRpbWV6b25lIjoiR01UKzMiLCJ0aW1lem9uZU9mZnNldCI6LTE4MCwidXNlcl9hZ2VudCI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzg4LjAuNDMyNC4xNTAgU2FmYXJpLzUzNy4zNiIsImxpc3RfcGx1Z2luIjoiQ2hyb21lIFBERiBQbHVnaW4sQ2hyb21lIFBERiBWaWV3ZXIsTmF0aXZlIENsaWVudCIsImNhbnZhc19jb2RlIjoiNzljMGQ2MDEiLCJ3ZWJnbF92ZW5kb3IiOiJHb29nbGUgSW5jLiIsIndlYmdsX3JlbmRlcmVyIjoiQU5HTEUgKG5vdXZlYXUsIE5WMTM3LCBPcGVuR0wgNC4zIGNvcmUpIiwiYXVkaW8iOiIxMjQuMDQzNDc3MzA1OTA5NjIiLCJwbGF0Zm9ybSI6IkxpbnV4IHg4Nl82NCIsIndlYl90aW1lem9uZSI6IkV1cm9wZS9Nb3Njb3ciLCJkZXZpY2VfbmFtZSI6IkNocm9tZSBWODguMC40MzI0LjE1MCAoTGludXgpIiwiZmluZ2VycHJpbnQiOiJkNjRhY2JjZjRhYTkyNTg2ZTY0MmIzZjVlNjNhMjZlMSIsImRldmljZV9pZCI6IiIsInJlbGF0ZWRfZGV2aWNlX2lkcyI6IiJ9"},
		"bnc-uuid":           {"56df8f9f-72d6-468c-8db4-384c1f8952f5"},
		"clienttype":         {"web"},
		"accept":             {"*/*"},
		"sec-fetch-site":     {"same-origin"},
		"sec-fetch-mode":     {"cors"},
		"sec-fetch-dest":     {"empty"},
		"referer":            {"https://c2c.binance.com/ru/fiatOrder"},
		"accept-language":    {"ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"},
		"cookie":             {`cid=kYj0jMtc; _h_desk_key=dc263ac915164800961f58c06a27b5c0; fiat-prefer-currency=RUB; sensorsdata2015jssdkcross={"distinct_id":"26751873","first_id":"17b3e53220161b-0114af2f618f7-3b710f51-2073600-17b3e5322026eb","props":{},"$device_id":"17b3e53220161b-0114af2f618f7-3b710f51-2073600-17b3e5322026eb"}; userPreferredCurrency=RUB_USD; bnc-uuid=227a61c6-a80a-449e-aa96-728f6e07aefc; source=referral; campaign=www.binance.com; _ga=GA1.2.1115138680.1629268511; home-ui-ab=B; lang=ru; gtId=90994c94-0d09-431e-b140-13b3e23a1f21; BNC_FV_KEY=31a42304c94aad2389868d0b9937af05cc576d72; BNC_FV_KEY_EXPIRE=1630481524019; cr00=EC26DC4F22D2357CA96C2805CE6507C5; d1og=web.26751873.FE330DCC7721BE9E6A56876CF800645B; r2o1=web.26751873.31EEDC99B66FB937B134CC5E122CCD82; f30l=web.26751873.43C5C4C84AAFE91838BF2FA3B28D7BAE; logined=y; p20t=web.26751873.53A52281FF89BE1483632B4B4D4A37F3`},
	}

	HeadersTelegram = map[string][]string{
		"content-type": {"application/json"},
	}
}
