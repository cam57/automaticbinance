package main

import (
	"fmt"
	"github.com/tebeka/selenium"
	"os"
	"time"
)

func main() {
	// Start a Selenium WebDriver server instance (if one is not already
	// running).
	const (
		// These paths will be different on your system.
		//seleniumPath    = "/home/anton/go/src/github.com/tebeka/selenium/vendor/selenium-server-standalone-3.9.1.jar"
		//geckoDriverPath = "/home/anton/go/src/github.com/tebeka/selenium/vendor/chromedriver"
		seleniumPath    = "/home/anton/go/src/github.com/tebeka/selenium/vendor/selenium-server.jar"
		geckoDriverPath = "/home/anton/go/src/github.com/tebeka/selenium/vendor/geckodriver"
		port            = 8080
	)
	opts := []selenium.ServiceOption{
		//selenium.StartFrameBuffer(),           // Start an X frame buffer for the browser to run in.
		selenium.GeckoDriver(geckoDriverPath), // Specify the path to GeckoDriver in order to use Firefox.
		selenium.Output(os.Stderr),            // Output debug information to STDERR.
	}
	selenium.SetDebug(false)
	service, err := selenium.NewSeleniumService(seleniumPath, port, opts...)
	if err != nil {
		panic(err) // panic is used only as an example and is not otherwise recommended.
	}
	defer service.Stop()

	// Connect to the WebDriver instance running locally.
	caps := selenium.Capabilities{"browserName": "firefox"}
	//caps := selenium.Capabilities{"browserName": "chrome"}
	//chrCaps := chrome.Capabilities{
	//	Path: "/home/anton/go/src/github.com/tebeka/selenium/vendor/chrome-linux/chrome",
	//	Args: []string{
	//		"--headless", // <<<
	//		"--no-sandbox",
	//		"--user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4537.0 Safari/537.36",
	//	},
	//}
	//caps.AddChrome(chrCaps)

	wdBinance, err := selenium.NewRemote(caps, fmt.Sprintf("http://127.0.0.1:%d/wd/hub", port))
	if err != nil {
		panic(err)
	}

	defer wdBinance.Quit()

	//// Navigate to the simple playground interface.
	////if err := wd.Get("http://play.golang.org/?simple=1"); err != nil {
	////	panic(err)
	////}
	////
	////// Get a reference to the text box containing code.
	////elem, err := wd.FindElement(selenium.ByCSSSelector, "#code")
	////if err != nil {
	////	panic(err)
	////}
	////// Remove the boilerplate code already in the text box.
	////if err := elem.Clear(); err != nil {
	////	panic(err)
	////}
	////
	////// Enter some new code in text box.
	////err = elem.SendKeys(`
	////	package main
	////	import "fmt"
	////
	////	func main() {
	////		fmt.Println("Hello WebDriver!")
	////	}
	////`)
	////if err != nil {
	////	panic(err)
	////}
	////
	////// Click the run button.
	////btn, err := wd.FindElement(selenium.ByCSSSelector, "#run")
	////if err != nil {
	////	panic(err)
	////}
	////if err := btn.Click(); err != nil {
	////	panic(err)
	////}
	////
	////// Wait for the program to finish running and get the output.
	////outputDiv, err := wd.FindElement(selenium.ByCSSSelector, "#output")
	////if err != nil {
	////	panic(err)
	////}
	////
	////var output string
	////for {
	////	output, err = outputDiv.Text()
	////	if err != nil {
	////		panic(err)
	////	}
	////	if output != "Waiting for remote server..." {
	////		break
	////	}
	////	time.Sleep(time.Millisecond * 10000)
	////}
	////
	////fmt.Printf("%s", strings.Replace(output, "\n\n", "\n", -1))
	////
	//////Example Output:
	//////Hello WebDriver!
	//////
	//////Program exited.
	//
	if err := wdBinance.Get("https://accounts.binance.com/ru/login"); err != nil {
		panic(err)
	}

	a := ""
	fmt.Print("Нажмите энтер: ")
	fmt.Fscan(os.Stdin, &a)

	if err := wdBinance.Get("https://c2c.binance.com/ru/fiatOrder"); err != nil {
		panic(err)
	}

	elemProcess, _ := wdBinance.FindElement(selenium.ByXPATH, "/html/body/div[1]/div[2]/main/div[2]/div/div/div[2]")
	elemProcess.Click()
	fmt.Print("Нажмите энтер: ")
	fmt.Fscan(os.Stdin, &a)
	/////*[@id="C2CordersList_btn_detail"]
	// /html/body/div[1]/div[2]/main/div[2]/div/div[1]/div[4]/div[3]/div[1]/div/div/div[2] pay method
	///html/body/div[1]/div[2]/main/div[2]/div/div[1]/div[4]/div[3]/div[2]/div/div[2]/div[2]/div card number
	///html/body/div[1]/div[2]/main/div[2]/div/div[1]/div[4]/div[3]/div[2]/div/div[1]/div[2]/div fio
	// /html/body/div[1]/div[2]/main/div[2]/div/div[1]/div[7]/div pay
	// /html/body/div[3]/div/div/div[3]/div[3]/button[1] confirm

	return

	//wdSber, err := selenium.NewRemote(caps, fmt.Sprintf("http://127.0.0.1:%d/wd/hub", port))
	//if err != nil {
	//	panic(err)
	//}
	//
	//defer wdSber.Quit()
	//
	//if err := wdSber.Get("https://online.sberbank.ru/CSAFront/index.do#/"); err != nil {
	//	panic(err)
	//}
	//fmt.Print("Нажмите энтер: ")
	//fmt.Fscan(os.Stdin, &a)

	wdTin, err := selenium.NewRemote(caps, fmt.Sprintf("http://127.0.0.1:%d/wd/hub", port))
	if err != nil {
		panic(err)
	}

	defer wdTin.Quit()

	if err := wdTin.Get("https://www.tinkoff.ru/login/"); err != nil {
		panic(err)
	}
	pressEnter()

	payButton, _ := wdTin.FindElement(selenium.ByCSSSelector, "body > div.application > div > div > div > div.PortalContainer__container_a2Jb-r > div.UILayoutPage__page_a3Jy6F > div:nth-child(2) > div.PlatformLayout__layoutPageComponent_c3HfJi > div > div.Container__container_a1CGWF > div > div.DashboardLayout__rightColumn_c2zJag > div.Payments__wrapper_a1x_DM > div.PaymentsBlocks__paysWrapper_b3qv2m > div:nth-child(2) > a > span > div > div > div")
	payButton.Click()
	time.Sleep(5 * time.Second)

	cardPayButton, _ := wdTin.FindElement(selenium.ByCSSSelector, "div.InnerGrid__item_2NJ4r:nth-child(2) > a:nth-child(1) > span:nth-child(1) > div:nth-child(1)")
	cardPayButton.Click()
	time.Sleep(5 * time.Second)
	pressEnter()

	cardNumberInput, _ := wdTin.FindElement(selenium.ByXPATH, "/html/body/div[1]/div/div[4]/div/div/div/section/div/div/form/div[4]/div[1]/div/div/div/span/span/div/div/div[1]/div/div/div/div/div/div/div/div/div[2]/div/input")
	cardNumberInput.SendKeys("45626")
	time.Sleep(5 * time.Second)

	priceInput, _ := wdTin.FindElement(selenium.ByXPATH, "/html/body/div[1]/div/div[4]/div/div/div/section/div/div/form/div[5]/div/div/div[1]/div/div[1]/div/div/div[2]/div/input")
	priceInput.SendKeys("69")
	time.Sleep(5 * time.Second)
	pressEnter()

	submitButton, _ := wdTin.FindElement(selenium.ByXPATH, "/html/body/div[1]/div/div[4]/div/div/div/section/div/div/form/div[7]/div[1]/button")
	submitButton.Click()
	time.Sleep(5 * time.Second)
	pressEnter()
}

func pressEnter() {
	enter := ""
	fmt.Print("Нажмите энтер: ")
	fmt.Fscan(os.Stdin, &enter)
}
