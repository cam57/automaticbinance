package main

import (
	"bitbucket.org/cam57/automaticbinance"
	model "bitbucket.org/cam57/automaticbinance/internal/app/struct"
	"bytes"
	"os"
	"time"

	//"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
)

type P2pwall struct {
	Asset string              `json:"asset"`
	Free  model.CustomFloat64 `json:"free"`
}

type P2pResp struct {
	Code    string     `json:"code"`
	Data    []*P2pwall `json:"data"`
	Success bool       `json:"success"`
}

type DepthResp struct {
	Asks [][]model.CustomFloat64 `json:"asks"`
}

func main() {
	//app := "lsdfgh"
	//
	//arg0 := "-"
	//
	//cmd := exec.Command(app, arg0)
	//stdout, err := cmd.Output()
	//
	//if err != nil {
	//	fmt.Println(err.Error())
	//	return
	//}
	//
	//// Print the output
	//fmt.Println(string(stdout))
	for true {
		client := http.Client{}
		requestP2pWall, err := http.NewRequest("GET", "https://www.binance.com/bapi/c2c/v1/private/c2c/asset/balance", nil)
		if err != nil {
			log.Fatalln(err)
		}

		requestP2pWall.Header = automaticbinance.HeadersBinance

		resp, err := client.Do(requestP2pWall)
		if err != nil {
			log.Println(100)
			log.Println(err)
		}

		var resultP2pResp *P2pResp
		if err := json.NewDecoder(resp.Body).Decode(&resultP2pResp); err != nil {
			log.Println(106)
			log.Fatal(err)
		}
		log.Printf("%+v", resultP2pResp.Data)
		log.Printf("%+v", len(resultP2pResp.Data))

		if len(resultP2pResp.Data) == 0 {
			sendTelegram(`{"chat_id":"221523189","text":"Session close","parse_mode":"HTML"}`)
			os.Exit(0)
		}

		for i := 0; i < len(resultP2pResp.Data); i++ {

			if resultP2pResp.Data[i].Free.Float64 > 0 {
				println(resultP2pResp.Data[i].Asset, " free > 0 ", resultP2pResp.Data[i].Free.Float64)

				reqTransfer, err := http.NewRequest("POST", "https://www.binance.com/bapi/asset/v1/private/asset-service/wallet/transfer", bytes.NewBuffer([]byte(fmt.Sprintf(`{"amount":"%.10f","asset":"%v","kindType":"FIAT_MAIN"}`, resultP2pResp.Data[i].Free.Float64, resultP2pResp.Data[i].Asset))))
				if err != nil {
					log.Println(118)
					log.Fatal(err)
				}
				reqTransfer.Header = automaticbinance.HeadersBinance

				resp, err := client.Do(reqTransfer)
				if err != nil {
					log.Fatalln(err)
				}
				var resultTransfer map[string]interface{}
				if err := json.NewDecoder(resp.Body).Decode(&resultTransfer); err != nil {
					log.Println(128)
					log.Println(err)
					continue
				}
				log.Println(resultTransfer)
				if resultTransfer["success"].(bool) != true {
					log.Fatal("something went wrong")
				}

				if resultP2pResp.Data[i].Asset != "RUB" {

					reqDepth, err := http.NewRequest("GET", fmt.Sprintf("https://www.binance.com/api/v3/depth?symbol=%vRUB&limit=10", resultP2pResp.Data[i].Asset), nil)
					if err != nil {
						log.Println(141)
						log.Println(err)
						continue
					}
					reqDepth.Header = automaticbinance.HeadersBinance

					respDepth, err := client.Do(reqDepth)
					if err != nil {
						log.Println(148)
						log.Fatalln(err)
					}
					var resultDepth DepthResp
					if err := json.NewDecoder(respDepth.Body).Decode(&resultDepth); err != nil {
						log.Println("Ошибка глубины")
						log.Println(err)
						continue
					}
					log.Printf("%+v", resultDepth)
					println()

					if len(resultDepth.Asks) == 0 {
						log.Println(160)
						log.Fatal("result depth equal 0")
					}
					log.Println(resultDepth.Asks)
					var jsonSell string
					if resultP2pResp.Data[i].Asset == "BTC" {
						jsonSell = fmt.Sprintf(`{"side":"SELL","symbol":"%vRUB","quantity":"%.6f","price":"%.6f","type":"LIMIT"}`, resultP2pResp.Data[i].Asset, math.Floor(resultP2pResp.Data[i].Free.Float64*1000000)/1000000, resultDepth.Asks[0][0].Float64)
					} else if resultP2pResp.Data[i].Asset == "ETH" {
						jsonSell = fmt.Sprintf(`{"side":"SELL","symbol":"%vRUB","quantity":"%.4f","price":"%.2f","type":"LIMIT"}`, resultP2pResp.Data[i].Asset, math.Floor(resultP2pResp.Data[i].Free.Float64*10000)/10000, resultDepth.Asks[0][0].Float64)
					} else if resultP2pResp.Data[i].Asset == "BNB" {
						jsonSell = fmt.Sprintf(`{"side":"SELL","symbol":"%vRUB","quantity":"%.3f","price":"%.3f","type":"LIMIT"}`, resultP2pResp.Data[i].Asset, math.Floor(resultP2pResp.Data[i].Free.Float64*1000)/1000, resultDepth.Asks[0][0].Float64)
					} else if resultP2pResp.Data[i].Asset == "BUSD" {
						jsonSell = fmt.Sprintf(`{"side":"SELL","symbol":"%vRUB","quantity":"%.0f","price":"%.2f","type":"LIMIT"}`, resultP2pResp.Data[i].Asset, math.Floor(resultP2pResp.Data[i].Free.Float64*1000)/1000, resultDepth.Asks[0][0].Float64)
					} else if resultP2pResp.Data[i].Asset == "USDT" {
						jsonSell = fmt.Sprintf(`{"side":"SELL","symbol":"%vRUB","quantity":"%.0f","price":"%.2f","type":"LIMIT"}`, resultP2pResp.Data[i].Asset, math.Floor(resultP2pResp.Data[i].Free.Float64*1000)/1000, resultDepth.Asks[0][0].Float64)
					}
					log.Println(jsonSell)
					reqSell, err := http.NewRequest("POST", "https://www.binance.com/bapi/mbx/v1/private/mbxgateway/order/place", bytes.NewBuffer([]byte(jsonSell)))
					if err != nil {
						log.Println("Ошибка запроса селл")
						log.Println(err)
						continue
					}
					reqSell.Header = automaticbinance.HeadersBinance

					respSell, err := client.Do(reqSell)
					if err != nil {
						log.Fatalln(err)
					}

					var resultSell map[string]interface{}
					if err := json.NewDecoder(respSell.Body).Decode(&resultSell); err != nil {
						log.Println(resultSell, "Ошибка декодирования ордера")
						log.Println(err)
						continue
					}

					if resultSell["success"].(bool) != true {
						log.Println(resultSell)
						log.Println("Order не был выставлен")
						sendTelegram(`{"chat_id":"221523189","text":"Ордер не вытавлен ` + resultP2pResp.Data[i].Asset + `","parse_mode":"HTML"}`)
						continue
					}
					println("sell order place")
				}
			}
		}
		time.Sleep(30 * time.Second)
	}
}

func sendTelegram(body string) {
	client := http.Client{}
	sendTelegram, _ := http.NewRequest("GET", "https://api.telegram.org/bot1903500884:AAFPnx5QS5pUvis9ba9Ic878DsOkhLTfcgU/sendMessage", bytes.NewBuffer([]byte(body)))

	sendTelegram.Header = automaticbinance.HeadersTelegram

	client.Do(sendTelegram)
}
