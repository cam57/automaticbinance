package main

import (
	"bitbucket.org/cam57/automaticbinance"
	"bitbucket.org/cam57/automaticbinance/internal/app/struct"
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"time"
	//"time"
)

type data struct {
	Data []*_struct.Order `json:"data"`
}

type transactions struct {
	Data struct {
		Data []*automaticbinance.Transaction `json:"data"`
	} `json:"data"`
}

func main() {

	db, err := sql.Open("postgres", "host=localhost port=5433 dbname=dev_db user=root password=root sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}

	if err := db.Ping(); err != nil {
		log.Fatalln(err)
	}
	defer db.Close()

	lastOrderNumber := ""
	if err := db.QueryRow(`SELECT order_number FROM orders ORDER BY create_time DESC LIMIT 1`).Scan(&lastOrderNumber); err != nil && err != sql.ErrNoRows {
		log.Fatal(err)
	}

	page := 1
	execFor := true
	//jsonBody := `{"orderStatus":"4","page":%d,"rows":50}`
	//count := 0
	//var totalSell, total float64

	//for execFor {
	//	println("get orders")
	//	client := http.Client{}
	//	requestCompletedOrders, err := http.NewRequest("POST", "https://c2c.binance.com/bapi/c2c/v2/private/c2c/order-match/order-list", bytes.NewBuffer([]byte(fmt.Sprintf(jsonBody, page))))
	//	if err != nil {
	//		log.Fatalln(err)
	//	}
	//
	//	requestCompletedOrders.Header = automaticbinance.HeadersBinance
	//
	//	resp, err := client.Do(requestCompletedOrders)
	//	if err != nil {
	//		log.Println(40)
	//		log.Println(err)
	//	}
	//
	//	var resultOrders *data
	//	if err := json.NewDecoder(resp.Body).Decode(&resultOrders); err != nil {
	//		log.Println(resultOrders, "Ошибка декодирования ордера")
	//		log.Println(err)
	//		continue
	//	}
	//
	//	if len(resultOrders.Data) == 0 {
	//		execFor = false
	//		continue
	//	}
	//
	//	for i := 0; i < len(resultOrders.Data); i++ {
	//
	//		if resultOrders.Data[i].OrderNumber == lastOrderNumber {
	//			execFor = false
	//			break
	//		}
	//
	//		//fmt.Printf("%+v", resultOrders.Data[i])
	//		//os.Exit(0)
	//
	//		for iPay := 0; iPay < len(resultOrders.Data[i].PayMethods); iPay++ {
	//			for iFields := 0; iFields < len(resultOrders.Data[i].PayMethods[iPay].Fields); iFields++ {
	//				if resultOrders.Data[i].PayMethods[iPay].Fields[iFields].FieldContentType == "payee" {
	//					resultOrders.Data[i].PayMethods[iPay].Payee = resultOrders.Data[i].PayMethods[iPay].Fields[iFields].FieldValue
	//				} else if resultOrders.Data[i].PayMethods[iPay].Fields[iFields].FieldContentType == "pay_account" {
	//					resultOrders.Data[i].PayMethods[iPay].BankValue = resultOrders.Data[i].PayMethods[iPay].Fields[iFields].FieldValue
	//				} else if resultOrders.Data[i].PayMethods[iPay].Fields[iFields].FieldContentType == "bank" {
	//					resultOrders.Data[i].PayMethods[iPay].Bank = resultOrders.Data[i].PayMethods[iPay].Fields[iFields].FieldValue
	//				}
	//			}
	//
	//			if err := db.QueryRow(
	//				`SELECT id FROM pay_methods WHERE bank_value = $1 AND payee = $2`,
	//				resultOrders.Data[i].PayMethods[iPay].BankValue,
	//				resultOrders.Data[i].PayMethods[iPay].Payee,
	//			).Scan(&resultOrders.Data[i].PayMethods[iPay].Id); err != nil {
	//				if err == sql.ErrNoRows {
	//					err := db.QueryRow(
	//						`INSERT INTO pay_methods(identifier, payee, bank, bank_value)
	//								VALUES($1, $2, $3, $4) RETURNING id`,
	//						resultOrders.Data[i].PayMethods[iPay].Identifier,
	//						resultOrders.Data[i].PayMethods[iPay].Payee,
	//						resultOrders.Data[i].PayMethods[iPay].Bank,
	//						resultOrders.Data[i].PayMethods[iPay].BankValue,
	//					).Scan(&resultOrders.Data[i].PayMethods[iPay].Id)
	//					if err != nil {
	//						log.Fatal(err)
	//					}
	//				} else {
	//					log.Fatal(err)
	//				}
	//			}
	//
	//			resultOrders.Data[i].PayMethodId = resultOrders.Data[i].PayMethods[iPay].Id
	//
	//		}
	//
	//		err := db.QueryRow(
	//			`INSERT INTO orders(order_number, create_time, notify_pay_end_time, price, amount, asset, fiat, total_price, trade_type, pay_method_id)
	//						VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING id`,
	//			resultOrders.Data[i].OrderNumber,
	//			resultOrders.Data[i].CreateTime,
	//			resultOrders.Data[i].NotifyPayEndTime,
	//			resultOrders.Data[i].Price.Float64,
	//			resultOrders.Data[i].Amount.Float64,
	//			resultOrders.Data[i].Asset,
	//			resultOrders.Data[i].Fiat,
	//			resultOrders.Data[i].TotalPrice.Float64,
	//			resultOrders.Data[i].TradeType,
	//			resultOrders.Data[i].PayMethodId,
	//		).Scan(&resultOrders.Data[i].Id)
	//		if err != nil {
	//			log.Fatal(err)
	//		}
	//	}
	//
	//	page++
	//	time.Sleep(5 * time.Second)
	//}

	startDate := time.Now().AddDate(0, -3, 1).Format("2006-01-02T00:00:00Z")
	endDate := time.Now().AddDate(0, 0, 1).Format("2006-01-02T00:00:00Z")
	println(startDate, endDate)
	execFor = true
	page = 1
	lastOrderNo := ""
	if err := db.QueryRow(`SELECT order_no FROM transactions ORDER BY created_at DESC LIMIT 1`).Scan(&lastOrderNo); err != nil && err != sql.ErrNoRows {
		log.Fatal(err)
	}
	for execFor {
		println("get transaction")
		fmt.Printf(
			`{"businessType":"withdraw","page":%d,"rows":20,"startDate":"%v","endDate":"%v"}`,
			page,
			startDate,
			endDate,
		)
		client := http.Client{}
		requestTransaction, err := http.NewRequest(
			"POST", "https://www.binance.com/bapi/fiat/v1/friendly/fiatpayment/transactions/get-order-history",
			bytes.NewBuffer(
				[]byte(
					fmt.Sprintf(
						`{"businessType":"withdraw","page":%d,"rows":20,"startDate":"%v","endDate":"%v"}`,
						page,
						startDate,
						endDate,
					),
				),
			),
		)
		if err != nil {
			log.Fatalln(err)
		}

		requestTransaction.Header = automaticbinance.HeadersBinance

		resp, err := client.Do(requestTransaction)
		if err != nil {
			log.Println(40)
			log.Println(err)
		}

		var result *transactions
		if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
			log.Println(result, "Ошибка декодирования ордера")
			log.Println(err)
			continue
		}

		if len(result.Data.Data) == 0 {
			break
		}

		if len(result.Data.Data) < 20 {
			execFor = false
		}

		for i := 0; i < len(result.Data.Data); i++ {
			if lastOrderNo == result.Data.Data[i].OrderNo {
				execFor = false
				break
			}

			err := db.QueryRow(
				`INSERT INTO transactions(order_no, fiat_currency, status, amount, created_at, updated_at)
						VALUES($1, $2, $3, $4, $5, $6) RETURNING id`,
				result.Data.Data[i].OrderNo,
				result.Data.Data[i].FiatCurrency,
				result.Data.Data[i].Status,
				result.Data.Data[i].Amount.Float64,
				result.Data.Data[i].CreatedAt,
				result.Data.Data[i].UpdatedAt,
			).Scan(&result.Data.Data[i].Id)
			if err != nil {
				log.Fatal(err)
			}
			log.Printf("%v\n", result.Data.Data[i])
		}
		page++
	}
}
