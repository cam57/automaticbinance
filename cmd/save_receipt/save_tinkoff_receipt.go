package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"github.com/tebeka/selenium"
	"os"
)

func main() {

	months := []string{
		"января",
		"февраля",
		"марта",
		"апреля",
		"мая",
		"июня",
		"июля",
		"августа",
		"сентября",
		"октября",
		"ноября",
		"декабря",
	}

	db, err := sql.Open("postgres", "host=localhost port=5433 dbname=dev_db user=root password=root sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}

	if err := db.Ping(); err != nil {
		log.Fatalln(err)
	}
	defer db.Close()
	// Start a Selenium WebDriver server instance (if one is not already
	// running).
	const (
		seleniumPath    = "/home/anton/go/src/github.com/tebeka/selenium/vendor/selenium-server.jar"
		geckoDriverPath = "/home/anton/go/src/github.com/tebeka/selenium/vendor/geckodriver"
		port            = 8080
	)

	outfile, err := os.OpenFile("logs/app.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println(err)
	}

	opts := []selenium.ServiceOption{
		selenium.GeckoDriver(geckoDriverPath), // Specify the path to GeckoDriver in order to use Firefox.
		selenium.Output(outfile),              // Output debug information to STDERR.
	}
	selenium.SetDebug(false)

	service, err := selenium.NewSeleniumService(seleniumPath, port, opts...)
	if err != nil {
		panic(err) // panic is used only as an example and is not otherwise recommended.
	}
	defer service.Stop()

	// Connect to the WebDriver instance running locally.
	caps := selenium.Capabilities{"browserName": "firefox"}

	wdTinkoff, err := selenium.NewRemote(caps, fmt.Sprintf("http://127.0.0.1:%d/wd/hub", port))
	if err != nil {
		panic(err)
	}
	wdTinkoff.SetPageLoadTimeout(10 * time.Second)
	wdTinkoff.SetAsyncScriptTimeout(5 * time.Second)

	defer wdTinkoff.Quit()

	if err := wdTinkoff.Get("https://www.tinkoff.ru/login/"); err != nil {
		panic(err)
	}

	enter := ""
	fmt.Print("Login\n")
	fmt.Fscan(os.Stdin, &enter)
	time.Sleep(10 * time.Second)

	result := "\n"

	exec := true

	//wdTinkoff.SetImplicitWaitTimeout(10 * time.Second)
	time.Sleep(1 * time.Second)
	for exec {
		///html/body/div[1]/div/div/div/div[1]/div[2]/div[2]/div[2]/div/div[4]/div/div/div[1]/div/div/div/div[1]/div/div[2]/div
		elements, err := wdTinkoff.FindElements(selenium.ByXPATH, "/html/body/div[1]/div/div/div/div[1]/div[2]/div[2]/div[2]/div/div[5]/div/div/div[1]/div/div[1]/div/div")
		if err != nil {
			panic(err)
		}

		for i := 0; i < len(elements); i++ {

			result += fmt.Sprintf("i=%v\n", i+1)
			result += fmt.Sprintf(`/html/body/div[1]/div/div/div/div[1]/div[2]/div[2]/div[2]/div/div[5]/div/div/div[1]/div/div[1]/div/div[%d]/div/div[1]/div/div/div/div/div/span`, i+1)
			elementCategory, _ := elements[i].FindElement(
				selenium.ByXPATH,
				fmt.Sprintf(`/html/body/div[1]/div/div/div/div[1]/div[2]/div[2]/div[2]/div/div[5]/div/div/div[1]/div/div[1]/div/div[%d]/div/div[1]/div/div/div/div/div/span`, i+1),
			)
			if elementCategory == nil {
				continue
			}
			text, _ := elementCategory.Text()
			if text != "Переводы" {
				result += "=" + text + "don't translations\n"
				continue
			}
			result += "translation=" + text + "\n"

			elementLogo, _ := elements[i].FindElement(
				selenium.ByXPATH,
				fmt.Sprintf("/html/body/div[1]/div/div/div/div[1]/div[2]/div[2]/div[2]/div/div[5]/div/div/div[1]/div/div[1]/div/div[%d]/div/div[2]/div/div[1]/div/div", i+1),
			)
			style, _ := elementLogo.GetAttribute("style")
			if style == "background-image: url(\"https://brands-prod.cdn-tinkoff.ru/general_logo/sber.png\");" || style == `style="background-image: url("https://brands-prod.cdn-tinkoff.ru/general_logo/raiffeisen.png");"` {
				result += style + "sberbank or raif\n"
				println("sberbank or raif\n")
				continue
			}
			result += "style=" + style + "\n"

			if err := elements[i].Click(); err == nil {

				id := 0

				elementPrice, _ := elements[i].FindElement(
					selenium.ByXPATH,
					"/html/body/div[4]/div[2]/div/div/div/div/div[2]/div[1]/div[3]/section[1]/div/div[2]/p/span",
				)
				priceStr, _ := elementPrice.Text()
				priceStr = strings.ReplaceAll(priceStr, "₽", "")
				priceStr = strings.ReplaceAll(priceStr, "−", "")
				priceStr = strings.ReplaceAll(priceStr, " ", "")
				priceStr = strings.ReplaceAll(priceStr, ",", ".")
				var price float32
				json.Unmarshal([]byte(priceStr[1:len(priceStr)-2]), &price)
				result += "priceStr=" + priceStr + "=\n"

				elementDate, _ := elements[i].FindElement(
					selenium.ByXPATH,
					`/html/body/div[4]/div[2]/div/div/div/div/div[2]/div[1]/p/span[1]`,
				)
				date, _ := elementDate.Text()
				for iMonths := 0; iMonths < len(months); iMonths++ {
					if iMonths < 9 {
						date = strings.ReplaceAll(date, months[iMonths], fmt.Sprintf("0%d", iMonths+1))
					} else {
						date = strings.ReplaceAll(date, months[iMonths], fmt.Sprintf("%d", iMonths+1))
					}
				}
				date = strings.ReplaceAll(date, ", ", " ")
				date = strings.Replace(date, " ", ".", 2)
				date = date + ":00"

				numberReceipt, _ := wdTinkoff.CurrentURL()
				numberReceipt = numberReceipt[58:] //https://www.tinkoff.ru/events/feed/?auth=null&operationId=16601823880

				elementName, _ := elements[i].FindElement(
					selenium.ByXPATH,
					"/html/body/div[4]/div[2]/div/div/div/div/div[2]/div[1]/div[3]/section[1]/div/p",
				)
				userName, _ := elementName.Text()

				result += fmt.Sprintf("%v   price=%v  date=%v  number receipt=%v username=%v \n", id, price, date, numberReceipt, userName)

				if save := pressEnter("Save\n"); save {
					result += "save\n"

					//if err := db.QueryRow(
					// `INSERT INTO receipts(number_receipt, price, full_name, bank, file_name, receipt_time)
					//		VALUES($1, $2, $3, $4, $5, $6) RETURNING id`,
					//).Scan(&id); err != nil {
					//}
				} else {
					result += "don't save"
					result += "\n\n\n"
				}
				elementClose, _ := elements[i].FindElement(selenium.ByXPATH, `//button[@data-qa-type="details-card-close"]`)
				elementClose.Click()
				result += "\n\n\n"
				time.Sleep(2 * time.Second)
			} else {
				result += err.Error() + "\n"
			}
		}

		if !pressEnter("Continue?") {
			exec = false
		}
	}
	println(result)
}

func pressEnter(text string) bool {
	enter := ""
	fmt.Print(text)
	fmt.Fscan(os.Stdin, &enter)
	println("enter=", enter, "=")
	if enter == "Y" || enter == "y" {
		return true
	}

	return false
}
