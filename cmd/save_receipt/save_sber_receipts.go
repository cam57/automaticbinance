package main

import (
	"fmt"
	"github.com/tebeka/selenium"
	"os"
	"time"
)

func main() {
	const (
		seleniumPath    = "/home/anton/go/src/github.com/tebeka/selenium/vendor/selenium-server.jar"
		geckoDriverPath = "/home/anton/go/src/github.com/tebeka/selenium/vendor/geckodriver"
		port            = 8080
	)

	outfile, err := os.OpenFile("logs/app.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println(err)
	}

	opts := []selenium.ServiceOption{
		selenium.GeckoDriver(geckoDriverPath), // Specify the path to GeckoDriver in order to use Firefox.
		selenium.Output(outfile),              // Output debug information to STDERR.
	}
	selenium.SetDebug(false)

	service, err := selenium.NewSeleniumService(seleniumPath, port, opts...)
	if err != nil {
		panic(err) // panic is used only as an example and is not otherwise recommended.
	}
	defer service.Stop()

	// Connect to the WebDriver instance running locally.
	caps := selenium.Capabilities{"browserName": "firefox"}

	wdSber, err := selenium.NewRemote(caps, fmt.Sprintf("http://127.0.0.1:%d/wd/hub", port))
	if err != nil {
		panic(err)
	}
	wdSber.SetPageLoadTimeout(10 * time.Second)
	wdSber.SetAsyncScriptTimeout(5 * time.Second)

	defer wdSber.Quit()

	if err := wdSber.Get("https://online.sberbank.ru/C"); err != nil {
		panic(err)
	}

	enter := ""
	fmt.Print("Login\n")
	fmt.Fscan(os.Stdin, &enter)
	time.Sleep(10 * time.Second)

}
