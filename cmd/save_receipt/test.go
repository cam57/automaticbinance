package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

func main() {
	priceStr := "−30 000,00 ₽"
	priceStr = strings.ReplaceAll(priceStr, "₽", "")
	priceStr = strings.ReplaceAll(priceStr, "−", "")
	priceStr = strings.ReplaceAll(priceStr, " ", "")
	priceStr = strings.ReplaceAll(priceStr, ",", ".")
	var price float32
	json.Unmarshal([]byte(priceStr), &price)
	fmt.Println(price, "5465")

	months := []string{
		"января",
		"февраля",
		"марта",
		"апреля",
		"мая",
		"июня",
		"июля",
		"августа",
		"сентября",
		"октября",
		"ноября",
		"декабря",
	}
	date := "27 октября 2021, 14:48"
	for iMonths := 0; iMonths < len(months); iMonths++ {
		if iMonths < 9 {
			date = strings.ReplaceAll(date, months[iMonths], fmt.Sprintf("0%d", iMonths+1))
		} else {
			date = strings.ReplaceAll(date, months[iMonths], fmt.Sprintf("%d", iMonths+1))
		}
	}
	date = strings.ReplaceAll(date, ", ", " ")
	date = strings.Replace(date, " ", ".", 2)
	date = date + ":00"
	println(date)
}
