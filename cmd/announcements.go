package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
)

type customFloat64 struct {
	Float64 float64
}

func (cf *customFloat64) UnmarshalJSON(data []byte) error {
	if data[0] == 34 {
		err := json.Unmarshal(data[1:len(data)-1], &cf.Float64)
		if err != nil {
			return errors.New("CustomFloat64: UnmarshalJSON: " + err.Error())
		}
	} else {
		err := json.Unmarshal(data, &cf.Float64)
		if err != nil {
			return errors.New("CustomFloat64: UnmarshalJSON: " + err.Error())
		}
	}
	return nil
}

func (cf *customFloat64) MarshalJSON() ([]byte, error) {
	j, err := json.Marshal(cf.Float64)
	return j, err
}

type Announcements struct {
	Adv struct {
		Price        customFloat64 `json:"price"`
		TradeMethods []struct {
			TradeMethodName string `json:"tradeMethodName"`
		} `json:"tradeMethods"`
	} `json:"adv"`
	Advertiser struct {
		NickName string `json:"nickName"`
	} `json:"advertiser"`
}

type Resp struct {
	Code    string          `json:"code"`
	Data    []Announcements `json:"data"`
	Success bool            `json:"success"`
}

func main() {

	coins := []string{"BTC", "BNB", "ETH"}
	for i := 0; len(coins) > i; i++ {
		client := http.Client{}
		requestP2pWall, err := http.NewRequest("POST", "https://p2p.binance.com/bapi/c2c/v2/friendly/c2c/adv/search", bytes.NewBuffer([]byte(fmt.Sprintf(`{"page":1,"rows":10,"payTypes":[],"asset":"%v","tradeType":"SELL","fiat":"RUB","publisherType":null,"merchantCheck":false,"transAmount":"500"}`, coins[i]))))
		if err != nil {
			log.Fatalln(err)
		}

		headers := map[string][]string{
			"authority":          {"www.binance.com"}, //const
			"x-trace-id":         {"6e7d6a51-83a6-4a09-9f98-b64af2d0de7c"},
			"csrftoken":          {"b984e5f677edba1c41b67782acfe27ea"},
			"x-ui-request-trace": {"6e7d6a51-83a6-4a09-9f98-b64af2d0de7c"},
			"user-agent":         {"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                //const
			"content-type":       {"application/json"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         //const
			"lang":               {"ru"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       //const
			"fvideo-id":          {"31a42304c94aad2389868d0b9937af05cc576d72"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 //const
			"device-info":        {"eyJzY3JlZW5fcmVzb2x1dGlvbiI6IjE5MjAsMTA4MCIsImF2YWlsYWJsZV9zY3JlZW5fcmVzb2x1dGlvbiI6IjE5MjAsMTA4MCIsInN5c3RlbV92ZXJzaW9uIjoiTGludXggeDg2XzY0IiwiYnJhbmRfbW9kZWwiOiJ1bmtub3duIiwic3lzdGVtX2xhbmciOiJydS1SVSIsInRpbWV6b25lIjoiR01UKzMiLCJ0aW1lem9uZU9mZnNldCI6LTE4MCwidXNlcl9hZ2VudCI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzg4LjAuNDMyNC4xNTAgU2FmYXJpLzUzNy4zNiIsImxpc3RfcGx1Z2luIjoiQ2hyb21lIFBERiBQbHVnaW4sQ2hyb21lIFBERiBWaWV3ZXIsTmF0aXZlIENsaWVudCIsImNhbnZhc19jb2RlIjoiNzljMGQ2MDEiLCJ3ZWJnbF92ZW5kb3IiOiJHb29nbGUgSW5jLiIsIndlYmdsX3JlbmRlcmVyIjoiQU5HTEUgKG5vdXZlYXUsIE5WMTM3LCBPcGVuR0wgNC4zIGNvcmUpIiwiYXVkaW8iOiIxMjQuMDQzNDc3MzA1OTA5NjIiLCJwbGF0Zm9ybSI6IkxpbnV4IHg4Nl82NCIsIndlYl90aW1lem9uZSI6IkV1cm9wZS9Nb3Njb3ciLCJkZXZpY2VfbmFtZSI6IkNocm9tZSBWODguMC40MzI0LjE1MCAoTGludXgpIiwiZmluZ2VycHJpbnQiOiJkNjRhY2JjZjRhYTkyNTg2ZTY0MmIzZjVlNjNhMjZlMSIsImRldmljZV9pZCI6IiIsInJlbGF0ZWRfZGV2aWNlX2lkcyI6IiJ9"}, //const
			"bnc-uuid":           {"56df8f9f-72d6-468c-8db4-384c1f8952f5"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     //const
			"clienttype":         {"web"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      //const
			"accept":             {"*/*"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      //const
			"sec-fetch-site":     {"same-origin"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              //const
			"sec-fetch-mode":     {"cors"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     //const
			"sec-fetch-dest":     {"empty"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    //const
			"referer":            {"https://www.binance.com/ru/"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              //const
			"accept-language":    {"ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      //const
			"cookie":             {"cid=gXg351i8; _ga=GA1.2.847018868.1613826897; bnc-uuid=56df8f9f-72d6-468c-8db4-384c1f8952f5; source=organic; campaign=www.google.com; fiat-prefer-currency=RUB; defaultMarketTab=spot; userPreferredCurrency=RUB_USD; _h_desk_key=dc263ac915164800961f58c06a27b5c0; logined=y; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22177bf943f255fc-0ad796f4948dd2-3b710f51-2073600-177bf943f26422%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%7D%2C%22%24device_id%22%3A%22177bf943f255fc-0ad796f4948dd2-3b710f51-2073600-177bf943f26422%22%7D; BNC_FV_KEY=31a42304c94aad2389868d0b9937af05cc576d72; BNC_FV_KEY_EXPIRE=1624434626742; lang=ru; gtId=df1c8387-54cd-4f26-bed2-be6d02844217; cr00=9BCC801D9C9E1174132366E8F65DE6A8; d1og=web.26751873.04E27F668E247F7DA765D16CF940440C; r2o1=web.26751873.8F780F11C3440C6BE90314D038E92E76; f30l=web.26751873.CF2B93EC7673002F7BF3E470B9EDCB60; logined=y; p20t=web.26751873.532F7B36CDBFAD14D3386E462C84BF46"},
		}

		requestP2pWall.Header = headers

		resp, err := client.Do(requestP2pWall)
		if err != nil {
			log.Println(100)
			log.Fatalln(err)
		}

		var result *Resp
		if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
			log.Println(106)
			log.Fatal(err)
		}

		println(coins[i])
		for i2 := 0; len(result.Data) > i2; i2++ {
			log.Printf("%+v", result.Data[i2].Advertiser.NickName)
			log.Printf("%+v", result.Data[i2].Adv.Price)
			log.Printf("%+v", result.Data[i2].Adv.TradeMethods)
			println()
		}
	}
}
