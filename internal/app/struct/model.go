package _struct

import (
	"encoding/json"
	"errors"
)

type CustomFloat64 struct {
	Float64 float64
}

func (cf *CustomFloat64) UnmarshalJSON(data []byte) error {
	if data[0] == 34 {
		err := json.Unmarshal(data[1:len(data)-1], &cf.Float64)
		if err != nil {
			return errors.New("CustomFloat64: UnmarshalJSON: " + err.Error())
		}
	} else {
		err := json.Unmarshal(data, &cf.Float64)
		if err != nil {
			return errors.New("CustomFloat64: UnmarshalJSON: " + err.Error())
		}
	}
	return nil
}

func (cf *CustomFloat64) MarshalJSON() ([]byte, error) {
	j, err := json.Marshal(cf.Float64)
	return j, err
}

type CustomInt64 struct {
	Int64 int64
}

func (ci *CustomInt64) UnmarshalJSON(data []byte) error {
	if data[0] == 34 {
		err := json.Unmarshal(data[1:len(data)-1], &ci.Int64)
		if err != nil {
			return errors.New("CustomInt64: UnmarshalJSON: " + err.Error())
		}
	} else {
		err := json.Unmarshal(data, &ci.Int64)
		if err != nil {
			return errors.New("CustomInt64: UnmarshalJSON: " + err.Error())
		}
	}
	return nil
}

func (ci *CustomInt64) MarshalJSON() ([]byte, error) {
	j, err := json.Marshal(ci.Int64)
	return j, err
}

type Order struct {
	Id               int64         `json:"id"`
	OrderNumber      string        `json:"orderNumber"`
	CreateTime       int64         `json:"createTime"`
	NotifyPayEndTime int64         `json:"notifyPayEndTime"`
	Price            CustomFloat64 `json:"price"`
	Amount           CustomFloat64 `json:"amount"`
	Asset            string        `json:"asset"`
	Fiat             string        `json:"fiat"`
	TotalPrice       CustomFloat64 `json:"totalPrice"`
	TradeType        string        `json:"tradeType"`
	PayMethods       []*PayMethod  `json:"payMethods"`
	PayMethodId      int64
}

type PayMethod struct {
	Id         int64  `json:"id"`
	Identifier string `json:"identifier"`
	Fields     []struct {
		FieldContentType string `json:"fieldContentType"`
		FieldValue       string `json:"fieldValue"`
	} `json:"fields"`
	Payee     string
	Bank      string
	BankValue string
}
